module.exports = async function(context, commands) {
  console.log("running the script");
  const webdriver = context.selenium.webdriver;
  const driver = context.selenium.driver;

  const email = "testuserkelvin@gmail.com";
  const password = "S2iCgvXTLVdbYgV";
  const wordToCheck = "random";

  await commands.navigate("https://mail.google.com/");
  console.log("navigated to the website");

  await commands.wait.byPageToComplete();

  await commands.addText.bySelector(email, 'input[type="email"]');
  await commands.screenshot.take("email");
  console.log("inputed email");
  await commands.click.byIdAndWait("identifierNext");

  await commands.wait.byId("passwordNext", 10000);

  await commands.wait.byTime(5000);

  await commands.screenshot.take("before pass");

  await commands.addText.bySelector(password, 'input[name="password"]');

  await commands.screenshot.take("password");
  await commands.click.byXpath('//*[@id="passwordNext"]');
  console.log("inputed password");

  await commands.screenshot.take("password after");

  console.log("waiting to open email");
  let title = await driver.getTitle();
  console.log(`the title is ${title}`);

  await commands.wait.byTime(30000);

  await commands.screenshot.take("password after 30000 wait");

  console.log("waiting to open email");
  let title2 = await driver.getTitle();
  console.log(`the title is ${title2}`);

  await commands.wait.byXpath('//*[@id="aso_search_form_anchor"]', 50000);
  let title3 = await driver.getTitle();
  console.log(`the title is ${title3}`);

  await commands.click.bySelectorAndWait("span[email]");

  console.log("clicked first email");

  await commands.screenshot.take("body");

  const body = await driver.findElement(
    webdriver.By.xpath("//*[@id]/div[1]/div[2]/div[3]")
  );

  const bodyText = await body.getText();

  let doesWordExist = bodyText.includes(wordToCheck);

  doesWordExist
    ? console.log("Word exists in the first email")
    : console.log("Word does not exist in the first email");
};
