require("chromedriver");
const { Builder, By, Key, until } = require("selenium-webdriver");

const { email, password, wordToCheck } = require("./details.json");

(async function selGoogle() {
  let driver = await new Builder().forBrowser("chrome").build();
  try {
    await driver.get("https://mail.google.com/");
    await driver
      .findElement(By.css('input[type="email"]'))
      .sendKeys(email, Key.RETURN);
    await driver.wait(function() {
      return driver
        .findElement(By.css('input[type="password"]'))
        .getAttribute("aria-label")
        .then(function(label) {
          return label === "Enter your password";
        });
    });
    await driver
      .findElement(By.css('input[type="password"]'))
      .sendKeys(password, Key.RETURN);

    await driver.wait(until.titleContains(email));
    let title = await driver.getTitle();
    console.log(`the title is ${title}`);
    
    let firstEmail = await driver.findElement(
      By.xpath(
        "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div[6]/div/div[1]/div[3]/div/table/tbody/tr[1]"
      )
    );

    if (!firstEmail) {
      console.log("no first email");
      return;
    }

    await firstEmail.click();

    await driver.wait(
      until.elementLocated(
        By.xpath(
          "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[1]/table/tbody/tr/td/h3/span[1]/span[2]"
        )
      ),
      10000
    );

    const body = await driver.findElement(
      By.xpath(
        "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[3]"
      )
    );

    const bodyText = await body.getText();

    let doesWordExist = bodyText.includes(wordToCheck);

    doesWordExist
      ? console.log("Word exists in the first email")
      : console.log("Word does not exist in the first email");
  } finally {
    await driver.sleep(1000)
    await driver.quit();
  }
})();
